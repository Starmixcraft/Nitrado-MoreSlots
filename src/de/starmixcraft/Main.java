package de.starmixcraft;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import de.starmixcraft.listener.Pingevent;
import de.starmixcraft.listener.PlayerJoinEvent;

public class Main extends JavaPlugin{
	
	File config = new File(getDataFolder(), "/config.yml");
	
	public static YamlConfiguration cf;
	
	@Override
	public void onEnable() {
		if(!getDataFolder().exists()) {
		getDataFolder().mkdir();
		try { config.createNewFile(); } catch (IOException e) { e.printStackTrace(); }
		cf = YamlConfiguration.loadConfiguration(config);
		cf.set("maxSlots", 32);
		cf.set("kickMessage", "Keine freien slotz mehr!");
		try {
			cf.save(config);
		} catch (IOException e) {
			e.printStackTrace();
		}
		}else {
			cf = YamlConfiguration.loadConfiguration(config);
		}
		
		Bukkit.getPluginManager().registerEvents(new Pingevent(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinEvent(), this);
		
		
	}
	

}
