package de.starmixcraft.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import de.starmixcraft.Main;

public class Pingevent implements Listener{
	
	@EventHandler
	public void onPing(ServerListPingEvent e) {
		e.setMaxPlayers(Main.cf.getInt("maxSlots"));
	}

}
