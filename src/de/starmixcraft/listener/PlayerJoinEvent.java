package de.starmixcraft.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import de.starmixcraft.Main;

public class PlayerJoinEvent implements Listener{
	
	@EventHandler
	public void onJoin(PlayerLoginEvent e) {
		if(Main.cf.getInt("maxSlots") > Bukkit.getOnlinePlayers().size()) {
			e.allow();
		}else {
			e.disallow(null, Main.cf.getString("kickMessage"));
		}
	}

}
